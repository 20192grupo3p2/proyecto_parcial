#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "../include/util.h"
#include "../include/vida.h"

int fflag=0;
int cflag=0;
int gflag=0;
int sflag=0;
int iflag=0;
juego_de_vida lifegame;

void iniciar_partida(juego_de_vida *lifegame, long ncel);

int main(int argc, char **argv){
	int opt=0;
	long ncelulas=0;

	while((opt=getopt(argc,argv,"f:c:g:s:i:"))!=-1){
		switch(opt){
			case 'f':
				
				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					fflag=1;
					lifegame.filas=atol(optarg);
					break;
				}
				
			
			case 'c':
					
					if(atol(optarg)<0){
						printf("No se permiten numeros negativos..\n");
						return -1;
					}
					else{
						cflag=1;
						lifegame.columnas=atol(optarg);;
						break;
					}
					
			case 'g':
				
				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					gflag=1;
					lifegame.generaciones=atol(optarg);
					break;
				}
			case 's':

				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					sflag=1;		
					lifegame.tiempo_sleep=atol(optarg);
					break;
				}
			case 'i':

				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					iflag=1;
					ncelulas=atol(optarg);
					break;
				}

			case '?':
				default:
					printf("Argumento no valido.\n");
					printf("Uso correcto:\n");
					printf("%s -f # -c # -g # -s # -i #\n",argv[0]);
					printf("-f # -> número de filas del tablero.\n");
					printf("-c # -> número de columnas del tablero.\n");
					printf("-g # -> número de generaciones , si es <= 0 se considera juego continuo.\n");
					printf("-s # -> tiempo entre cada generación (microsegundos).\n");
					printf("-i # -> número de células iniciales en el tablero\n");
					return -1;
		}

	}
	if(argc>optind){
		printf("Valores exceden los argumentos dados.\n");
		printf("Uso correcto:\n");
		printf("%s -f # -c # -g # -s # -i #\n",argv[0]);
		printf("-f # -> número de filas del tablero.\n");
		printf("-c # -> número de columnas del tablero.\n");
		printf("-g # -> número de generaciones , si es <= 0 se considera juego continuo.\n");
		printf("-s # -> tiempo entre cada generación (microsegundos).\n");
		printf("-i # -> número de células iniciales en el tablero\n");
		return -1;
	}

	long total;
	total=lifegame.columnas*lifegame.filas;
	if(ncelulas>total){
		printf("Numero de celulas excede tamaño de campo  de simulación.\n numero de cellas iniciales -i debe cumplir -i<=fxc.\n ");
		printf("Uso correcto:\n");
		printf("%s -f # -c # -g # -s # -i #\n",argv[0]);
		printf("-f # -> número de filas del tablero.\n");
		printf("-c # -> número de columnas del tablero.\n");
		printf("-g # -> número de generaciones , si es <= 0 se considera juego continuo.\n");
		printf("-s # -> tiempo entre cada generación (microsegundos).\n");
		printf("-i # -> número de células iniciales en el tablero\n");
		return -1;
	}

	
	if(fflag && cflag && gflag && sflag && iflag){
		iniciar_partida(&lifegame,ncelulas);
	}else{
		printf("Faltan argumentos.\n");
		printf("Uso correcto:\n");
		printf("%s -f # -c # -g # -s # -i #\n",argv[0]);
		printf("-f # -> número de filas del tablero.\n");
		printf("-c # -> número de columnas del tablero.\n");
		printf("-g # -> número de generaciones , si es <= 0 se considera juego continuo.\n");
		printf("-s # -> tiempo entre cada generación (microsegundos).\n");
		printf("-i # -> número de células iniciales en el tablero\n");
		return -1;
	}

	return 0;
}

void iniciar_partida(juego_de_vida *lifegame, long ncel){

	lifegame->tablero=(char **) calloc(lifegame->filas,sizeof(char *));

	for(int i=0;i<lifegame->filas;i++){

		*(lifegame->tablero+i)=(char *)calloc(lifegame->columnas,sizeof(char));
	}

	llenar_matriz_azar(lifegame->tablero,lifegame->filas,lifegame->columnas,ncel);

	int ngen=0;

	while (ngen<lifegame->generaciones){

		dibujar_grilla(lifegame->tablero,lifegame->filas,lifegame->columnas);
		printf("\n");
		printf("Generacion %d\n",ngen);
		print_estadistics(lifegame->tablero,lifegame->filas,lifegame->columnas,lifegame->generaciones);
		char **nueva=next_generations(lifegame->tablero,lifegame->filas,lifegame->columnas);
		if(nueva==NULL){
			printf("Error al reservar memoria\n");
			break;
		}
		else{
		usleep(lifegame->tiempo_sleep);
		for(int j=0;j<lifegame->filas;j++){
			free(lifegame->tablero[j]);
		}
		free(lifegame->tablero);
		lifegame->tablero=nueva;
		ngen++;	}

	}


}




