#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "../include/util.h"
#include "../include/vida.h"


void dibujar_grilla(char **matriz, int fil, int col){		
	printf("\e[1;1H\e[2J");
	char *linea = malloc(col + 1);	//Char nulo al final
	for(int i = 0; i < fil; i++){
		memset(linea, ' ', col+1);
		linea[col] = 0;
		for(int j = 0; j < col; j++){
			if(matriz[i][j] == 0){
				linea[j] = ' ';
			}
			else if(matriz[i][j] == 1){
				linea[j] = 'x';			
			}
		}
		printf("%s", linea);
		printf("\n");
		
		fflush(stdout);

	}
	free(linea);

}


void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad){
	for(int i =0; i<fil; i++){
		memset(grilla[i], 0, col);	
	}
	srand(time(NULL));
	for(int i = 0; i < cantidad; i++){
		long rnd_fil = rand() % fil;	
		long rnd_col = rand() % col;
		
		//printf("%ld %ld\n", rnd_fil, rnd_col);
		if(grilla[rnd_fil][rnd_col] == 1){
			i--;
		}
		else{
			grilla[rnd_fil][rnd_col] = 1;
		}
		
	}
}

char **next_generations(char ** tablero,long filas, long columnas){

	char **nuevo=(char **) calloc(filas,sizeof(char *));
	if(nuevo==NULL){
		printf("Error al reservar memoria...\n");
	}
	else
	{
		/* code */
	
	
	for(int i=0;i<filas;i++){
		*(nuevo+i)=(char *)calloc(columnas,sizeof(char));
	}

	int vecinos_inter[8]={0};
	int vecino_esquina[3]={0};
	int vecino_bordes[5]={0};
	
	for( int i =0; i<filas;i++){
		for(int j=0;j<columnas;j++){
			//Esquinas
			if(i==0 && j==0){
				//printf("primera esquina");
				char celula=*(*(tablero+i)+j);
				char der= *(*(tablero+i)+j+1);
				char abajo=*(*(tablero+i+1)+j);
				char diag_ind=*(*(tablero+i+1)+j+1);
				//determinar_estadoCelula(char cell,int[] vecinos)
				vecino_esquina[0]=der;
				vecino_esquina[1]=abajo;
				vecino_esquina[2]=diag_ind;
				int newCelula;
				newCelula=determinar_estadoEsquina(celula,vecino_esquina);
				//tablero[i][j]=newCelula;
				nuevo[i][j]=newCelula;
				//memset(vecino_esquina, '0', 3);
				

			}
			if((i==0) && j==columnas-1){
				//printf("segunda esquina");
				char celula=*(*(tablero+i)+j);
				char izq= *(*(tablero+i)+j-1);
				char abajo=*(*(tablero+i+1)+j);
				char diag_ini=*(*(tablero+i+1)+j-1);
				vecino_esquina[0]=izq;
				vecino_esquina[1]=abajo;
				vecino_esquina[2]=diag_ini;
				int newCelula;
				newCelula=determinar_estadoEsquina(celula,vecino_esquina);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_esquina, '0', 8);
			}
			if(i==filas-1 && j==0){
				//printf("tercera esquina\n");
				char celula=*(*(tablero+i)+j);
				char arriba= *(*(tablero+i-1)+j);
				char derecha=*(*(tablero+i)+j+1);
				char diag_supd=*(*(tablero+i-1)+j+1);
				vecino_esquina[0]=arriba;
				vecino_esquina[1]=derecha;
				vecino_esquina[2]=diag_supd;
				int newCelula;
				newCelula=determinar_estadoEsquina(celula,vecino_esquina);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_esquina, '0',3);
				//determinar_estadoCelula(char cell,int[] vecinos)


			}

			if(i==filas-1 && j==columnas-1){
				//printf("cuarta esquina\n");
				char celula=*(*(tablero+i)+j);
				char arriba= *(*(tablero+i-1)+j);
				char izquierda=*(*(tablero+i)+j-1);
				char diag_supi=*(*(tablero+i-1)+j-1);
				//determinar_estadoCelula(char cell,int[] vecinos)
				vecino_esquina[0]=arriba;
				vecino_esquina[1]=izquierda;
				vecino_esquina[2]=diag_supi;
				int newCelula;
				newCelula=determinar_estadoEsquina(celula,vecino_esquina);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_esquina, '0', 3);
			}
			//bordes horizontales
			 
			if(i==0 && (j>0 && j<columnas-1) ){
				//printf("borde arriba\n");
				char celula=*(*(tablero+i)+j);
				char der= *(*(tablero+i)+j+1);
				char izq= *(*(tablero+i)+j-1);
				char abajo= *(*(tablero+i+1)+j);
				char diag_iniz= *(*(tablero+i+1)+j-1);
				char diag_ind= *(*(tablero+i+1)+j+1);
				vecino_bordes[0]=der;
				vecino_bordes[1]=abajo;
				vecino_bordes[2]=diag_ind;
				vecino_bordes[3]=izq;
				vecino_bordes[4]=diag_iniz;
				int newCelula;//determinar_estadoCelula(char cell,int[] vecinos)
				newCelula=determinar_estadoBordes(celula,vecino_bordes);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_esquina, 'z', 5);

			}
			if(i==filas-1 && (j>0 && j<columnas-1) ){
				//printf("borde abajo\n");
				char celula=*(*(tablero+i)+j);
				char der= *(*(tablero+i)+j+1);
				char izq= *(*(tablero+i)+j-1);
				char arriba= *(*(tablero+i-1)+j);
				char diag_supiz= *(*(tablero+i-1)+j-1);
				char diag_supd= *(*(tablero+i-1)+j+1);
				vecino_bordes[0]=der;
				vecino_bordes[1]=arriba;
				vecino_bordes[2]=diag_supd;
				vecino_bordes[3]=izq;
				vecino_bordes[4]=diag_supiz;
				int newCelula;
				newCelula=determinar_estadoBordes(celula,vecino_bordes);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_bordes, 'z',5);

			}
			//Bordes Verticales 
			if(j==0 && (i>0 && i< filas-1) ){
				//printf("borde izquierdo");
				char celula=*(*(tablero+i)+j);
				char der= *(*(tablero+i)+j+1);
				char arriba= *(*(tablero+i-1)+j);
				char abajo= *(*(tablero+i+1)+j);
				char diag_supd= *(*(tablero+i-1)+j+1);
				char diag_infd= *(*(tablero+i+1)+j+1);
				vecino_bordes[0]=der;
				vecino_bordes[1]=arriba;
				vecino_bordes[2]=diag_supd;
				vecino_bordes[3]=abajo;
				vecino_bordes[4]=diag_infd;
				int newCelula;
				newCelula=determinar_estadoBordes(celula,vecino_bordes);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
				//memset(vecino_bordes, '0',5);

			}
			if(j==columnas-1 && (i>0 && i< filas-1) ){
				//printf("borde derecho\n");
				char celula=*(*(tablero+i)+j);
				char izq= *(*(tablero+i)+j-1);
				char arriba= *(*(tablero+i-1)+j);
				char abajo= *(*(tablero+i+1)+j);
				char diag_supiz= *(*(tablero+i-1)+j-1);
				char diag_iniz= *(*(tablero+i+1)+j-1);
				vecino_bordes[0]=izq;
				vecino_bordes[1]=arriba;
				vecino_bordes[2]=diag_iniz;
				vecino_bordes[3]=abajo;
				vecino_bordes[4]=diag_supiz;
				int newCelula;
				newCelula=determinar_estadoBordes(celula,vecino_bordes);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
			//	memset(vecino_bordes, '0', 5);


			}
			// POSICIONES INTERMEDIAS
			if((i>0 && i<filas-1) && (j>0 && j<columnas-1)){
				//printf("borde intermedio");
				char celula=*(*(tablero+i)+j);
				char izq= *(*(tablero+i)+j-1);
				char arriba= *(*(tablero+i-1)+j);
				char abajo= *(*(tablero+i+1)+j);
				char derecha= *(*(tablero+i)+j+1);
				char diag_supiz= *(*(tablero+i-1)+j-1);
				char diag_iniz= *(*(tablero+i+1)+j-1);
				char diag_supd= *(*(tablero+i-1)+j+1);
				char diag_inde= *(*(tablero+i+1)+j+1);
				vecinos_inter[0]=izq;
				vecinos_inter[1]=arriba;
				vecinos_inter[2]=diag_iniz;
				vecinos_inter[3]=abajo;
				vecinos_inter[4]=diag_supiz;
				vecinos_inter[5]= derecha;
				vecinos_inter[6]=diag_supd;
				vecinos_inter[7]=diag_inde;
				int newCelula;
				newCelula=determinar_estadoInter(celula,vecinos_inter);
				nuevo[i][j]=newCelula;
				//tablero[i][j]=newCelula;
			}

		}

	}
	
	

	return nuevo;
	}
	return NULL;

}

int determinar_estadoEsquina(int cell,int vecinos[]){
	int vecinos_vivos=0;

	int celula; 
	for( int i = 0; i<3;i++){
		int v2=(vecinos[i]==1);
		if(v2){
			vecinos_vivos+=1;
		}
	}
	//Celula viva
	if(cell==1){
		if(vecinos_vivos<=1){
			//printf("\ncelula muerta por solead\n");
		celula= 0;
		}
		

		else if (vecinos_vivos>=4)
		{	celula=0;
		}
		else if ( vecinos_vivos==2 || vecinos_vivos==3){
			celula=1;
		}

	}
	//Celula muerta
	if(cell==0){
		if(vecinos_vivos==3){
		
			celula=1;

		}
		else
		{
			celula=0;
		}
		
	}
	return celula;


}

int determinar_estadoBordes(int cell,int vecinos[]){
	int vecinos_vivos=0;

	int celula; 
	for( int i = 0; i<5;i++){
		int v2=(vecinos[i]==1);
		if(v2){
			vecinos_vivos+=1;
		}
	}
	//Celula viva
	if(cell==1){
		if(vecinos_vivos<=1){

			celula= 0;
		}
		

		else if (vecinos_vivos>=4)
		{	celula=0;
		
		}
		else if ( vecinos_vivos==2 || vecinos_vivos==3){
			celula=1;
	
		}

	}
	//Celula muerta
	if(cell==0){
		if(vecinos_vivos==3){
			
			celula=1;

		}
		else
		{
			celula=0;
		}
		
	}
	return celula;


}
int determinar_estadoInter(int cell,int vecinos[]){
		int vecinos_vivos=0;

	int celula; 
	for( int i = 0; i<8;i++){
		int v2=(vecinos[i]==1);
		if(v2){
			vecinos_vivos+=1;
		}
	}
	//Celula viva
	if(cell==1){
		if(vecinos_vivos<=1){
			
		celula= 0;
		}
		

		else if (vecinos_vivos>=4)
		{	celula=0;

		}
		else if ( vecinos_vivos==2 || vecinos_vivos==3){
			celula=1;
		}

	}
	//Celula muerta
	if(cell==0){
		if(vecinos_vivos==3){
			celula=1;

		}
		else
		{
			celula=0;
		}
		
	}
	return celula;


}

void print_estadistics(char **tablero, long filas, long columnas, long generaciones){

	long totalcelulas= filas*columnas;
	long ncelulasvivas=0;
	long static totalcelulasvivas=0;
	long static totalcelulasmuertas=0;
	long promediocelulasvivas=0;
	long promediocelulasmuertas=0;
	
	for( int i =0; i<filas;i++){
		for(int j=0;j<columnas;j++){
			if(*(*(tablero+i)+j)==1){
				ncelulasvivas+=1;
			}
		}
	}

	long ncelularmuertas=totalcelulas-ncelulasvivas;
	
	totalcelulasvivas+=ncelulasvivas;

	totalcelulasmuertas+=ncelularmuertas;
	
	promediocelulasvivas=totalcelulasvivas/generaciones;

	promediocelulasmuertas=totalcelulasmuertas/generaciones;

	printf("\tNúmero de células vivas en esta generación: %ld\n",ncelulasvivas);
	printf("\tNúmero de células muertas en esta generación: %ld\n",ncelularmuertas);
	printf("\tNúmero de células vivas desde generación 0: %ld\n",totalcelulasvivas);
	printf("\tNúmero de células muertas desde generación 0: %ld\n",totalcelulasmuertas);
	printf("\tPromedio de células que murieron por generación: %ld\n",promediocelulasvivas);
	printf("\tPromedio de células que nacieron por generación: %ld\n",promediocelulasmuertas);
}

