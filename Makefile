bin/vida: obj/util.o obj/main.o
	gcc   $^ -o bin/vida -fsanitize=address,undefined

obj/main.o: src/main.c 
	gcc -Wall -c $^ -I include/ -o obj/main.o -g  

obj/util.o: src/util.c
	gcc -Wall -c $^ -I include/ -o obj/util.o -g 


.PHONY: clean
clean:
	rm bin/* obj/*
